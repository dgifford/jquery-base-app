/*
	App class
 */

class App extends BaseApp
{
	setDefaults()
	{
		super.setDefaults();

		this.results = {};
	}



	buttonClickFirst( e )
	{
		this.results['buttonClickFirst'] = e;
	}



	buttonClickSecond( e )
	{
		this.results['buttonClickSecond'] = e;
	}



	buttonClickWithParameters( e, p )
	{
		this.results['buttonClickWithParameters'] = [e, p];
	}



	divClick( e )
	{
		this.results['divClick'] = e;
	}



	parentClick( e )
	{
		this.results['parentClick'] = e;
	}



	formSubmit( e )
	{
		e.preventDefault();

		this.results['formSubmit'] = e;
	}



	inputChange( e )
	{
		this.results['inputChange'] = e;
	}
}
<!doctype html>
<html class="no-js" lang="">

<head>
	<meta charset="utf-8">
	<title>BaseApp Tests</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="csrf-token" content="1234567890">
	<meta name="csrf-token-alt" content="1234567890-alt">

	<!-- Dependencies -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
	<script	src="https://code.jquery.com/qunit/qunit-2.13.0.js"	integrity="sha256-nB5w0/qHO/+UJ9QHspTdKDm7/PGHNmBQ8fAY5gLKxHw="	crossorigin="anonymous"></script>

	<!-- Base App -->
	<script type="text/javascript" src="../src/BaseAppHelpers.js"></script>
	<script type="text/javascript" src="../src/BaseAppClass.js"></script>

	<!-- Test set up -->
	<script type="text/javascript" src="App.class.js?dsf"></script>
	<script type="text/javascript" src="DisplayTestResults.js"></script>

	<!-- Tests -->
	<script type="text/javascript" src="BaseAppHelpersTest.js"></script>
	<script type="text/javascript" src="BaseAppTest.js"></script>
	<script type="text/javascript" src="EventHandlingTest.js"></script>
	<script type="text/javascript" src="FetchTest.js"></script>
	<script type="text/javascript" src="ActionTest.js"></script>

</head>

	<body>
		<div id="app">

			<div id="server_response"></div>

			<div id="removable">Removed by JSON response after a few seconds</div>
			
			<div data-click="divClick">
				<button id="button_1" type="button" data-click="buttonClickFirst,buttonClickSecond">Click method on button</button>
			</div>
			
			<div data-click="parentClick">
				<button id="button_2" type="button">Click method on parent</button>
			</div>
			
			<div>
				<button id="button_3" type="button">No click method</button>
			</div>
			
			<div>
				<button id="button_4" type="button" data-click="buttonClickWithParameters" data-parameters="666">Parameters with click</button>
			</div>

			<form action="post" data-submit="formSubmit">
				<input name="input" data-change="inputChange">
			</form>

		</div>
		<pre></pre>
	</body>

</html>
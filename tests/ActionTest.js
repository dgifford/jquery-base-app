QUnit.module('App action()', function()
{
	QUnit.test('No callback provided', function(assert)
	{
		// Wait for response before performing test
		var done = assert.async();

		var app = new App;

		app.action( 'text-response.php' ).then( async function(response)
		{
			assert.equal( response.status, 200 );

			const str = await response.text();

			assert.equal( 'Text server response', str );

			done();
		});
	});



	QUnit.test('Text response is processed', function(assert)
	{
		// Wait for response before performing test
		var done = assert.async();

		var app = new App;

		app.actionHandler = async function( response )
		{
			assert.equal( response.status, 200 );

			const str = await response.text();

			assert.equal( 'Text server response', str );

			done();
		};

		app.action( 'text-response.php', null, 'actionHandler' );
	});



	QUnit.test('JSON response with content is processed', function(assert)
	{
		// Wait for response before performing test
		var done = assert.async();

		var app = new App;

		app.action( 'json-response-with-content.php' ).then(  function(json)
		{
			assert.equal( json.method, 'prepend' );
			assert.equal( json.selector, '#server_response' );
			assert.equal( json.content, '<span style=\"background-color:yellow\">HTML from server</span>' );

			done();
		});
	});



	QUnit.test('JSON response without content is processed', function(assert)
	{
		// Wait for response before performing test
		var done = assert.async();

		var app = new App;

		app.action( 'json-response.php' ).then(  function(json)
		{
			assert.equal( json.method, 'remove' );
			assert.equal( json.selector, '#removable' );

			done();
		});
	});



	QUnit.test('Not found action error processed', function(assert)
	{
		// Wait for response before performing test
		var done = assert.async();

		var app = new App;

		app.action( '404.php' ).then( async function(response)
		{
			assert.equal( response.status, 404 );

			done();
		});
	});
});
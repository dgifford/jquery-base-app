QUnit.module('BaseAppHelpers isString()', function()
{
	QUnit.test('returns true for a string or String Class', function(assert)
	{
		assert.true( isString('foo') );
		assert.true( isString( new String ) );
	});

	QUnit.test('returns false for non strings', function(assert)
	{
		assert.false( isString( null ) );
		assert.false( isString( 123 ) );
		assert.false( isString( true ) );
	});
});

QUnit.module('BaseAppHelpers divide()', function()
{
	QUnit.test('returns null for division by zero', function(assert)
	{
		assert.strictEqual( divide( 1, 0 ), null );
	});

	QUnit.test('returns correct division answer', function(assert)
	{
		assert.strictEqual( divide( 1, 2 ), 0.5 );
		assert.strictEqual( divide( 10, 2 ), 5 );
	});

	QUnit.test('returns custom result for division by zero', function(assert)
	{
		assert.strictEqual( divide( 1, 0, 'foo' ), 'foo' );
		assert.false( divide( 100, 0, false ) );
	});
});

QUnit.module('BaseAppHelpers splitString()', function()
{
	QUnit.test('splits at comma by default', function(assert)
	{
		assert.deepEqual( splitString( 'foo,bar' ), ['foo', 'bar'] );
		assert.deepEqual( splitString( 'foo,bar,woo' ), ['foo', 'bar', 'woo'] );
		assert.deepEqual( splitString( 'foo ,bar ' ), ['foo', 'bar'] );
		assert.deepEqual( splitString( ' foo ,bar	' ), ['foo', 'bar'] );
	});

	QUnit.test('splits at custom seperator', function(assert)
	{
		assert.deepEqual( splitString( 'foo|bar', '|' ), ['foo', 'bar'] );
		assert.deepEqual( splitString( 'foo,foo; bar ; woo', ';' ), ['foo,foo', 'bar', 'woo'] );
	});
});


QUnit.module('BaseAppHelpers formDataToJson()', function()
{
	QUnit.test('converts form data to JSON string', function(assert)
	{
		$('input').val('foo bar');
		
		assert.equal(
			formDataToJson( new FormData( document.querySelector('form') ) )
			, '{"input":"foo bar"}'
		);
	});
});


function output( str )
{
	$('pre').append( str+"\n" );
}



QUnit.testDone( function( details )
{
	output( `Module: ${details.module}, Test: ${details.name}, Passed: ${details.passed}/${details.total}` );

	if( details.failed > 0 )
	{
		output( `<span style="color:red">Failed: ${details.failed}/${details.total}</span>` );
	}

	if( details.skipped > 0 )
	{
		output( `<span style="color:orange">Skipped: ${details.skipped}/${details.total}</span>` );
	}
});



QUnit.log(function( details )
{
	if ( details.result )
	{
		return;
	}

	var loc = details.module + ": " + details.name + ": ",
	output = "FAILED: " + loc + ( details.message ? details.message + ", " : "" );

	if ( details.actual )
	{
		output += "expected: " + details.expected + ", actual: " + details.actual;
	}

	if ( details.source )
	{
		output += ", " + details.source;
	}
	
	console.log( output );
});
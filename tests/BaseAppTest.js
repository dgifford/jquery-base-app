QUnit.module('BaseApp constructor()', function()
{
	QUnit.test('Defaults set', function(assert)
	{
		var app = new App;

		assert.equal( app.root, '#app' );
	});

	QUnit.test('Parameters set and defaults over-ridden', function(assert)
	{
		var app = new App(
		{
			"foo":"bar",
			"root":"#new_root",
		});

		assert.equal( app.foo, 'bar' );
		assert.equal( app.root, '#new_root' );
	});

});



QUnit.module('BaseApp URLs', function()
{
	QUnit.test('Set base_url', function(assert)
	{
		var app = new App({"base_url":"http://example.com"});

		assert.true( app.base_url instanceof URL );

		assert.equal( app.base_url.href, 'http://example.com/' );

		var app = new App({"base_url":"http://example.com/"});

		assert.equal( app.base_url.href, 'http://example.com/' );
	});

	QUnit.test('Get absolute URL within app', function(assert)
	{
		var app = new App({"base_url":"http://example.com"});

		assert.equal( app.url(), 'http://example.com/' );

		assert.equal( app.url('/foo'), 'http://example.com/foo' );

		assert.equal( app.url('/foo'), 'http://example.com/foo' );

		assert.equal( app.url('/foo'), 'http://example.com/foo' );

		assert.equal( app.url('/foo/'), 'http://example.com/foo/' );

		assert.equal( app.url('foo/bar'), 'http://example.com/foo/bar' );

		assert.equal( app.url('/foo/bar'), 'http://example.com/foo/bar' );
	});

});
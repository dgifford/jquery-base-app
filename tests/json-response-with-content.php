<?php
header('Content-Type: application/json');

echo json_encode(
[
	'method' => 'prepend',
	'selector' => '#server_response',
	'content' => '<span style="background-color:yellow">HTML from server</span>',
]);
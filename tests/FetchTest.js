
QUnit.module('CSRF token', function()
{
	QUnit.test('Can retrieve token from Meta tag', function(assert)
	{
		var app = new App;

		assert.equal( app.getCsrfToken(), '1234567890' );

		assert.equal( app.getMetaCsrfToken(), '1234567890' );

		assert.equal( app.getMetaCsrfToken('csrf-token-alt'), '1234567890-alt' );
	});

	QUnit.test('Can retrieve token from App', function(assert)
	{
		var app = new App({'csrf_token': 'foo'});

		assert.equal( app.getCsrfToken(), 'foo' );
	});
});



QUnit.module('App fetch()', function()
{
	QUnit.test('Returns a promise', function(assert)
	{
		var app = new App;

		var f = app.fetch( 'post-target.php' );

		assert.true( f instanceof Promise );
	});

	QUnit.test('Get request returns a 200 response', function(assert)
	{
		var app = new App;

		// Wait for response before performing test
		var done = assert.async();

		var f = app.fetch( 'index.php', {'method':'get'} )
		.then(function(response)
		{
			assert.equal( response.status, 200 );

			done();
		});
	});

	QUnit.test('Get method returns a 200 response', function(assert)
	{
		var app = new App;

		// Wait for response before performing test
		var done = assert.async();

		var f = app.get( 'index.php' )
		.then(function(response)
		{
			assert.equal( response.status, 200 );

			done();
		});
	});
});
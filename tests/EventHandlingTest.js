QUnit.module('EventHandling clickhandler()', function()
{
	QUnit.test('Multiple button click handlers called', function(assert)
	{
		var app = new App;

		assert.true( $.isEmptyObject( app.results ) );

		$('#button_1').click();

		assert.equal( app.results['buttonClickFirst'].type, 'click' );

		assert.equal( app.results['buttonClickSecond'].type, 'click' );
	});



	QUnit.test('Parent button click handlers called', function(assert)
	{
		var app = new App;

		assert.true( $.isEmptyObject( app.results ) );

		$('#button_2').click();

		assert.equal( app.results['parentClick'].type, 'click' );
	});



	QUnit.test('No click handlers called', function(assert)
	{
		var app = new App;

		assert.true( $.isEmptyObject( app.results ) );

		$('#button_3').click();

		assert.true( $.isEmptyObject( app.results ) );
	});



	QUnit.test('Click handler called and parameters passed', function(assert)
	{
		var app = new App;

		assert.true( $.isEmptyObject( app.results ) );

		$('#button_4').click();

		assert.equal( app.results['buttonClickWithParameters'][0].type, 'click' );

		assert.equal( app.results['buttonClickWithParameters'][1], '666' );
	});
});



QUnit.module('EventHandling submitHandler()', function()
{
	QUnit.test('Submit handlers called', function(assert)
	{
		var app = new App;

		assert.true( $.isEmptyObject( app.results ) );

		$('form').submit();

		assert.equal( app.results['formSubmit'].type, 'submit' );
	});
});



QUnit.module('EventHandling changeHandler()', function()
{
	QUnit.test('Change handlers called', function(assert)
	{
		var app = new App;

		assert.true( $.isEmptyObject( app.results ) );

		$('input').val('foo').trigger('change');

		assert.equal( app.results['inputChange'].type, 'change' );
	});
});

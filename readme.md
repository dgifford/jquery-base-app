# jQuery Base App

JavaScript App class providing a base for simple apps using Jquery. It uses an element present on page load as the root element for the app, so that events on dynamically generated content can be handled.

The class should be extended to create an App class, see the example ```App.class.js``` file.

## Event Handling

Click, submit and change events are sent to the closest App method defined by an HTML attribute, i.e ```data-EVENT_NAME```. See jQuery ```closest()``` for how these attributes are identified.

Optional parameters can be passed to the App method by including a ```data-parameters``` attribute.

Event handling is initiated when the ```BaseApp``` class is instantiated using the ```
eventHandler()``` method.

### Examples 

#### Method attached directly to element

```
<button data-click="clickHandlingMethod">Click me</button>
```

#### Method with parameters

```
<button data-click="clickHandlingMethod" data-parameters="foo">Click me</button>
```

#### Method on parent element

```
<div data-click="clickHandlingMethod" data-parameters="foo">
	<button>Click me</button>
</div>
```

## XHR requests

The class includes an ascynchronous ```action()``` method for performing XHR requests to a server.

The method should be passed a path below the App URL, some data and a callback. The callback can be:

 - A name of a method in the App (string)
 - Names of several methods in the App (Array of strings)
 - An anonymous function
 - Undefined

If the callback is undefined and a JSON object is returned containing the keys 'method', 'selector' and, optionally, 'content', a jQuery method is automatically called.

This response from the server will append a message to the element identified by ```#messages```

```
{
	"method":"append",
	"selector":"#messages",
	"content":"<p>Here is a message<\/p>"
}
```
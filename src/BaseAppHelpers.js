/*
	Boolean checks from
	https://www.webbjocke.com/javascript-check-data-types/
*/

// Returns if a value is a string
function isString (value) {
return typeof value === 'string' || value instanceof String;
};



// Returns if a value is really a number
function isNumber (value) {
return typeof value === 'number' && isFinite(value);
};


// Returns if a value is an array
function isArray (value) {
return value && typeof value === 'object' && value.constructor === Array;
};



// Returns if a value is a function
function isFunction (value) {
return typeof value === 'function';
};



// Returns if a value is an object
function isObject (value) {
return value && typeof value === 'object' && value.constructor === Object;
};



// Returns if a value is null
function isNull (value) {
return value === null;
};



// Returns if a value is undefined
function isUndefined (value) {
return typeof value === 'undefined';
};



// Returns if a value is defined
function isDefined (value) {
return !isUndefined(value);
};



// Returns if a value is a boolean
function isBoolean (value) {
return typeof value === 'boolean';
};



/**
 * Divide and account for diision by zero
 * error. Divide a by b. If b = 0 return zr
 * 
 * @param  number a  Numerator
 * @param  number b  Denominator
 * @param  mixed  zr Returned if / 0
 * @return mixed
 */
function divide( a, b, zr )
{
	if( isUndefined(zr) )
	{
		zr = null;
	}

	if (0 + b == 0)
	{
		return zr;
	}
	else
	{
		return a / b;
	}
}



/**
 * Convert an object into form data.
 * 
 * @param  Object obj
 * @return FormData
 */
function objectToFormData( obj )
{
	var form_data = new FormData();

	for( var key in obj )
	{
		form_data.append(key, obj[key]);
	}

	return form_data;
}



/**
 * Convert formData object into JSON
 * https://stackoverflow.com/questions/41431322/how-to-convert-formdata-html5-object-to-json
 * @param  {[type]} formData [description]
 * @return {[type]}          [description]
 */
function formDataToJson( formData )
{
	var object = {};

	formData.forEach((value, key) =>
	{
		// Reflect.has in favor of: object.hasOwnProperty(key)
		if(!Reflect.has(object, key))
		{
			object[key] = value;
			return;
		}

		if(!Array.isArray(object[key]))
		{
			object[key] = [object[key]];
		}

		object[key].push(value);
	});

	return JSON.stringify(object);
}



/**
 * Split a string into an array and trim
 * whitespace on each element.
 *
**/
function splitString( str, seperator )
{
	if( isUndefined( seperator ) )
	{
		var seperator = ',';
	}

	arr = str.split( seperator );

	return str.split( seperator ).map( s => s.trim() );
}



/**
 * Converts a string to Title case.
 * 
 * @param  {[type]} str [description]
 * @return {[type]}     [description]
 */
function toTitleCase(str)
{
	return str.replace(/(?:^|\s)\w/g, function(match)
	{
		return match.toUpperCase();
	});
}


// https://stackoverflow.com/questions/6122571/simple-non-secure-hash-function-for-javascript
String.prototype.hashCode = function() {
    var hash = 0;
    if (this.length == 0) {
        return hash;
    }
    for (var i = 0; i < this.length; i++) {
        var char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}
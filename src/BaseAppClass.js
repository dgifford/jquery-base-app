/*
	Base app class
 */

class BaseApp
{
	/**
	 * Constructor
	 */
	constructor( properties ) 
	{
		// Set default properties
		this.setDefaults();

		// Use passed properties
		Object.assign( this, properties );

		// Create a URL object for the App
		this.setBaseUrl( this.base_url );

		// Set up event handlers
		this.setEventsHandled( this.events_handled );
	}



	/**
	 * Set default properties
	 */
	setDefaults()
	{
		// The root element selector string
		this.root = '#app';

		// Fetch defaults, see https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
		this.fetch_parameters =
		{
			method: 'POST',
			mode: 'cors',
			cache: 'no-cache',
			credentials: 'same-origin',
			headers:
			{
				'Content-Type': 'application/json'
			},
			referrerPolicy: 'no-referrer',
		};

		// Events handled by the App
		this.events_handled =
		[
			'click',
			'submit',
			'change'
		];
	}



	/**
	 * Set the events that will be handled when
	 * they hit the root element.
	 */
	setEventsHandled( events )
	{
		if( isArray(events) )
		{
			for(const event of events)
			{
				this.eventHandler(event);
			}
		}
	}



	/*
		Create a URL object for the app using
		the given url string or the current URL.
	*/
	setBaseUrl( base_url )
	{
		if( !isString( base_url ) )
		{
			base_url = window.location.href;
		}
		
		this.base_url = new URL( base_url );
	}



	/*
		Get absolute URL below base_url
	*/
	url( path )
	{
		if( isUndefined(path) )
		{
			return this.base_url.href;
		}

		return new URL( path, this.base_url.href ).href;
	}



	/**
	 * Handle events by calling methods defined
	 * in closest (see jQuery closest())
	 * data-EVENT_NAME attribute
	 * 
	 */
	eventHandler( name )
	{
		var self = this;

		$( self.root ).on( name, function( event )
		{
			// Get click handler and parameters
			var str = $(event.target).closest('[data-'+name+']').attr('data-'+name);
			var parameters = $(event.target).closest('[data-parameters]').attr('data-parameters');

			self.callEventMethods( str, event, parameters );
		})
	}



	/*
		Call an event handling method in the app if it exists.
	*/
	callEventMethodIfExists( method, event, parameters )
	{
		if( isUndefined( parameters ) )
		{
			parameters = null;
		}

		if( isFunction( this[ method ] ) )
		{
			this[ method ]( event, parameters );
		}
	}



	/**
	 * Call a string of methods sending
	 * each the same set of parameters.
	**/
	callEventMethods( str, event, parameters )
	{
		if( !isUndefined( str ) )
		{
			// Split into methods
			var methods = splitString( str );

			for( let method of methods )
			{
				this.callEventMethodIfExists( method, event, parameters );
			}
		}
	}



	/*
		Get the CSRF token from a meta tag in the header
	*/
	getMetaCsrfToken( name )
	{
		if( isUndefined(name) )
		{
			var name = 'csrf-token';
		}

		if( $('meta[name="'+name+'"]').length > 0 )
		{
			return $('meta[name="'+name+'"]').attr('content');
		}
	}



	/*
		Get CSRF token from app property or fallback to meta tag.
	*/
	getCsrfToken()
	{
		if( !isUndefined( this.csrf_token ) )
		{
			return this.csrf_token;
		}

		return this.getMetaCsrfToken();
	}



	/**
	 * Generate a fetch object combining passed parameters
	 * with the app defaults
	 */
	fetch( url, parameters )
	{
		var p = this.fetch_parameters;
		return fetch( url, Object.assign( p, parameters ) );
	}



	/*
		Perform an action by posting data to the server and
		returning the response to a callback function.

		By default, the data is posted withn the same origin.

		If a callback is not included and a JSON response contain
		the properties 'method' and 'data', the method in the
		returned data is called automatically.

		Callbacks must be asynchronous functions.

		https://dmitripavlutin.com/javascript-fetch-async-await/
	*/
	async action( path, data, callback )
	{
		var self = this,
			headers = {'Content-Type': 'application/json'},
			csrf_token = this.getCsrfToken();

		if( isString( csrf_token ) )
		{
			headers['X-CSRF-TOKEN'] = csrf_token;
		}

		const response = await self.fetch(
			self.url( path ), 
			{
				headers: headers,
				body: JSON.stringify(data)
			}
		);

		// Check for error (HTTP status not 200-299)
		if( !response.ok )
		{
			return self.actionError( response );
		}

		// Pass response to method within App
		if( isString( callback ) )
		{
			self[callback](response);
		}
		// Pass response to several methods within App
		else if( isArray( callback ) )
		{
			callback.forEach( el => self[el](response) );
		}
		// Callback is an anonymous funtion
		else if( isFunction( callback ) )
		{
			callback(response);
		}
		// No Callback & JSON returned
		else if( isUndefined( callback ) && response.headers.get('content-type') == 'application/json' )
		{
			const json = await response.json();

			self.processJsonResponse( json );

			return json;
		}

		return response;
	}



	/**
	 * Async get request
	 * 
	 */
	get( path, callback )
	{
		return this.fetch( path, {'method':'get'}, callback );
	}



	/*
		Default processor for JSON data - call jQuery function
		if JSON contains right keys. Element selection
		is within app root.
	*/
	processJsonResponse( obj )
	{
		if( !isUndefined(obj.selector) )
		{
			var jq = $( this.root ).find(obj.selector);

			if( isUndefined(obj.content) )
			{
				const content = null;
			}
			else
			{
				const content = obj.content;
			}

			if( !isUndefined(obj.method) )
			{
				jq[obj.method]( obj.content )
			}
		}
	}



	/**
	 * Process an action error
	 */
	actionError( response )
	{
		return response;
	}
}